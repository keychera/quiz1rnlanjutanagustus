import React, { useState, createContext } from 'react'
import { StyleSheet, View, Text, FlatList } from 'react-native'

export const RootContext = createContext();

const ContextAPI = () => {

  const [name, setName] = useState([
    {
      name: 'Zakky Muhammad Fajar',
      position: 'Trainer 1 React Native Lanjutan'
    },
    {
      name: 'Mukhlis Hanafi',
      position: 'Trainer 2 React Native Lanjutan'
    }
  ])

  return (
    <RootContext.Provider value={{ name }}>
      <RootContext.Consumer>
        {({ name }) => (
          <View style={styles.container}>
            <Text style={styles.title}>Daftar Nama Trainer</Text>
            <FlatList
              data={name}
              renderItem={({ item }) => (
                <View style={styles.itemRow}>
                  <Text style={styles.itemName}>{item.name}</Text>
                  <Text style={styles.itemPos}>{item.position}</Text>
                </View>
              )}
              keyExtractor={item => item.name}
            />
          </View>
        )}
      </RootContext.Consumer>
    </RootContext.Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  title: {
    fontSize: 20
  },
  itemRow: {
    flex: 1,
    flexDirection: "column",
    marginTop: 15
  },
  itemName: {
    fontWeight: "bold"
  },
  itemPos: {
    fontStyle: "italic"
  }
})

export default ContextAPI;
